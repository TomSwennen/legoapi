﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.WebSockets;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.Threading;
using System.Diagnostics;

namespace LegoAPI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public readonly HttpClient client = new HttpClient();
        public List<MiniFig> list = new List<MiniFig>();
        public string FullStringFile;

        string Serial;
        string Name;
        int Parts;
        string imageUrl;
        string linkUrl;
        int pagenumber = 1;

        BitmapImage legoBeeld;
        BitmapImage legoLogo;
        BitmapImage splash;

        string target;
        
        public MainWindow()
        {
            InitializeComponent();
            CreateCustomSplash();
            CreateLogo();
            FillList();
            
        }

        async Task CreateCustomSplash()
        {
            //Creates custom SplashScreen until the application has fully loaded itself and its lists.

            splash = new BitmapImage(new Uri(@"/Images/LegoLoading.gif", UriKind.RelativeOrAbsolute));
            SplashScreen.Source = splash;
            await FillList();
            SplashScreen.Visibility = Visibility.Hidden;
        }

        async Task FillList()
        {
            // Connects to Rebrickable API, gets all minifigs and creates the objects from the result.

            for(int page =1; page<13; page++)
            { 
            HttpResponseMessage response = await client.GetAsync($"https://rebrickable.com/api/v3/lego/minifigs/?page_count=1000&key=17335346e9fcb7b056bad76445fed4e0&page={page}");
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();

            var resultObjects = AllChildren(JObject.Parse(responseBody))
            .First(c => c.Type == JTokenType.Array && c.Path.Contains("results"))
            .Children<JObject>();

                foreach (JObject result in resultObjects)
                {
                    int i = 1;
                    foreach (JProperty property in result.Properties())
                    {
                        switch (i)
                        {
                            case 1: Serial = property.Value.ToString(); break;
                            case 2: Name = property.Value.ToString(); break;
                            case 3: Parts = Convert.ToInt32(property.Value); break;
                            case 4: imageUrl = property.Value.ToString(); break;
                            case 5: linkUrl = property.Value.ToString(); break;
                        }
                        i++;
                    }

                    MiniFig ToeTeVoegen = new MiniFig(Serial, Name, Parts, imageUrl, linkUrl);
                    list.Add(ToeTeVoegen);
                    lstLegoLijst.Items.Add(ToeTeVoegen.Name);
                    comBox.Items.Add(ToeTeVoegen.Name);
                }
                static IEnumerable<JToken> AllChildren(JToken json)
                {
                    foreach (var c in json.Children())
                    {
                        yield return c;
                        foreach (var cc in AllChildren(c))
                        {
                            yield return cc;
                        }
                    }
                }
                Thread.Sleep(500);
            }
            lstLegoLijst.SelectedIndex = 0;
            comBox.SelectedIndex = 0;
        }

        void ShowImage(int index)
        {
            // Changes and shows the image based on the selected object in the list.

            try
            {
                string afbeeldingslocatie = list[index].ImageLink.ToString();
                legoBeeld = new BitmapImage(new Uri(afbeeldingslocatie, UriKind.RelativeOrAbsolute));
                imgBeeld.Source = legoBeeld;
            }
            catch
            {
                legoBeeld = new BitmapImage(new Uri(@"/Images/Image404.jpg", UriKind.RelativeOrAbsolute));
                imgBeeld.Source = legoBeeld;
            }
            string parts = list[index].Parts.ToString();
            txtParts.Text = $"Number of parts: {parts}";

        }

        void CreateLogo()
        {
            // Creates the BrickBlock-logo

            string logolocatie = @"C:\ProjectenWPL1\LegoApiApp\LegoAPI\Images\BrickBlockLogo_v001.png";
            legoLogo = new BitmapImage(new Uri(logolocatie, UriKind.RelativeOrAbsolute));
            imgLogo.Source = legoLogo;
        }

        void lstLegoLijst_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ShowImage(lstLegoLijst.SelectedIndex);
            txtResultaat.Text = list[lstLegoLijst.SelectedIndex].LegoNaam();
            comBox.SelectedIndex = lstLegoLijst.SelectedIndex;
        }

        private void comBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            txtResultaat.Text = list[comBox.SelectedIndex].LegoNaam();
            lstLegoLijst.SelectedIndex = comBox.SelectedIndex;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // Opens a new browserwindow and opens the link to the Rebrickable page of the selected minifig.

            System.Diagnostics.Process.Start(new ProcessStartInfo
            {
                FileName = $"{list[lstLegoLijst.SelectedIndex].UrlLink.ToString()}",
                UseShellExecute = true
            });
        }
    }

}
