﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegoAPI
{
    public class MiniFig
    {
        private string set_num;
        private string name;
        private int num_parts;
        private string set_img_url;
        private string set_url;


        public string Serial
        {
            get { return set_num; }
            set { set_num = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Parts
        {
            get { return num_parts; }
            set { num_parts = value; }
        }

        public string ImageLink
        {
            get { return set_img_url; }
            set { set_img_url = value; }
        }

        public string UrlLink
        {
            get { return set_url; }
            set { set_url = value; }
        }

        public MiniFig(string serial, string name, int parts, string image, string url)
        {
            Serial = serial;
            Name = name;
            Parts = parts;
            ImageLink = image;
            UrlLink = url;
        }

        public MiniFig(string serial, string name)
        {
            Serial= serial;
            Name= name;
        }

        public string LegoNaam()
        {
            return this.Name.ToString();
        }

        public string LegoNameInfo()
        {
            return $"Serial number: {Serial}" + Environment.NewLine +
                    $"Name: {Name}" + Environment.NewLine;
        }
    }

}


